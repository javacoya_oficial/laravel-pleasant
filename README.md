Laravel Pleasant
============

Trait for Laravel Eloquent models to allow easy implementation of a Youtube-like "thumbs up / thumbs down" control.

Borrows _heavily_ from [Robert Conner's Laravel Likeable](https://github.com/rtconner/laravel-likeable) MIT-licensed plugin. I don't know if I really respected the license (probably not), but there's no misappropiation intended and I sort of wanted to try my hand quickly in componentizing an app. I will make it right as soon as I have the time to read up on Laravel and Composer package development best practices and the MIT license itself. Promise.

PS: no anonymous liking/disliking for now, so be sure to `Auth::check()` before use. 

PPS: Also, re-liking or re-disliking an item works as a toggle: liking an already liked item will actually unlike it.

PPPS: Laravel 4.2. Sorry guys ¯\\\_(ツ)\_/¯


#### Composer Install

    "require": {
        "archenemy/laravel-pleasant": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:archenemy/laravel-pleasant.git"
        }
    ]

#### Setup the service provider on `config\app.php`
    
    'providers' => array(
        ...
        'Archenemy\Pleasant\PleasantServiceProvider',
    ),

#### Run the migrations

	php artisan migrate --package=archenemy/laravel-pleasant
	
#### Publish the assets and views

    php artisan asset:publish archenemy/laravel-pleasant
    php artisan view:publish archenemy/laravel-pleasant


#### Setup your models

(Remember to implement `getLikeUrl()` and `getDislikeUrl()`. Most of the time, `return Request::url().'/like'` and `return Request::url().'/dislike'` counterpart are enough to get it rolling.)

    class Item extends \Eloquent implements \Archenemy\Pleasant\PleasantInterface {
        use Archenemy\Pleasant\PleasantTrait;
    }

#### Sample Usage

Setup some `/like` and `/dislike` routes for your entities, call your LikeController actions and do as you see fit. 

    $item->like(); // like the item for current user
    $item->dislike(); // dislike the item for current user
    $item->unlike(); // remove like from the item so it doesn't count towards likes or dislikes
    
    $item->likeCount(); // get count of likes
    $item->dislikeCount(); // get count of dislikes

    $item->likes; // Iterable collection of likes
    $item->liked(); // check if currently logged in user liked the item
    
    Item::whereLiked($user_id)->get(); // find only items liked by the user.

#### Included View

I have included a small partial view with a ajax widget. It assumes you have included the assets, and that you're working with Bootstrap and JQuery. 

    View::make('laravel-pleasant::partials/pleasant',['pleasant' => $itemtobeliked]);
    	
The included javascript expects a json object
    
    {
        "likes": 50,
        "dislikes": 12
    }

#### Credits
 - Fernando Gómez - http://fgomez.me
 - Robert Conner - http://smartersoftware.net

### Enjoy