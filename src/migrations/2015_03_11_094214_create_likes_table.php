<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pleasant_likes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('likeable_id');
			$table->string('likeable_type');
			$table->integer('user_id')->index();
			$table->boolean('value')->default(true);
			$table->timestamps();
			$table->unique(['likeable_id', 'likeable_type', 'user_id'], 'pleasant_likes_unique');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pleasant_likes');
	}

}
