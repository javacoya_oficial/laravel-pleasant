<?php namespace Archenemy\Pleasant;

trait PleasantTrait {

    public function scopeWhereLiked($query, $user_id=null) {
        if(is_null($user_id)) {
            $user_id = $this->loggedInUserId();
        }

        return $query->whereHas('likes', function($q) use($user_id) {
            $q->where('user_id', '=', $user_id);
        });
    }

    public function likes() {
        return $this->morphMany('\Archenemy\Pleasant\Like', 'likeable');
    }

    public function likeCount() {
        $likes = $this->likes->filter(function($like) {
           return $like->value == true;
        });
        return $likes->count();
    }

    public function dislikeCount() {
        $dislikes = $this->likes->filter(function($like) {
            return $like->value == false;
        });
        return $dislikes->count();
    }

    public function registerLike($likeValue = true, $user_id = null) {
        \Log::info(__FUNCTION__);

        if (is_null($user_id)) {
            $user_id = $this->loggedInUserId();
        }
        \Log::info('USERID '.$user_id);



        $like = $this->likes()->where('user_id', '=', $user_id)->first();
        // are we registering the same vote?
        \Log::info($like);
        if ($like) {

            if ($likeValue == $like->value) {
                $this->unlike($user_id);
                return;
            }

            $like->value = $likeValue;
            $this->likes()->update($like->toArray());
        } else {
            $like = new Like();
            $like->user_id = $user_id;
            $like->value = $likeValue;
            $this->likes()->save($like);
        }

    }

    public function like($user_id = null) {
        $this->registerLike(true, $user_id);
    }

    public function dislike($user_id = null) {
        \Log::info(__FUNCTION__);
        $this->registerLike(false, $user_id);
    }

    public function unlike($user_id = null) {
        if (is_null($user_id)) {
            $user_id = $this->loggedInUserId();
        }

        if ($user_id) {
            $like = $this->likes()->where('user_id', '=', $user_id)->first();
            if(!$like) return;
            $like->delete();
        }
    }

    public function liked($user_id = null) {
        if (is_null($user_id)) {
            $user_id = $this->loggedInUserId();
        }

        return (bool) $this->likes()->where('user_id', '=', $user_id)->count();
    }

    public function loggedInUserId() {
        return \Auth::id();
    }
}
