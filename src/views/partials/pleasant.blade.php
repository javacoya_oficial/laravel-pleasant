<div class="panel panel-primary panel-valoraciones">
  <div class="panel-heading" >
     <span class="glyphicon glyphicon-signal"></span> {{ Lang::get('site.panel-valoraciones')}}
   </div>
   <div class="panel-body center-block text-center" id="panel-valoraciones-body">








<div class="btn-group" role="group">
  <button id="{{ Str::slug($pleasant->getLikeUrl()) }}" class="btn btn-primary">
    <span class="pleasant-icon-spinner animate-spin spinner"></span><span class="glyphicon glyphicon-thumbs-up"></span>
    <span class="action-desc" style="display:none;">{{ Lang::get('site.like') }}</span>
    <span class="badge {{ Str::slug($pleasant->getLikeUrl()) }}-count">
      {{ $pleasant->likeCount() }}
    </span>
    @if($pleasant->ajax or true)
    <script type="text/javascript">
      jQuery(document).ready(function() {
        jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }} .spinner").hide();

        jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }}").on('click',function(e){
          e.preventDefault();
          jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }} .glyphicon").hide();
          jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }} .spinner").show();

          jQuery.ajax("{{ $pleasant->getLikeUrl() }}", {
            method: 'POST',
            dataType: 'json',
            success: function(response) {
              jQuery('.{{ Str::slug($pleasant->getLikeUrl()) }}-count').html(response.likes);
              jQuery('.{{ Str::slug($pleasant->getDislikeUrl()) }}-count').html(response.dislikes);

            },
            complete: function() {
              jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }} .glyphicon").show();
              jQuery("#{{ Str::slug($pleasant->getLikeUrl()) }} .spinner").hide();
            },
          });
        });
      });
    </script>
    @endif
  </button>

  <button id="{{ Str::slug($pleasant->getDislikeUrl()) }}" class="btn btn-danger">
    <span class="pleasant-icon-spinner animate-spin spinner"></span><span class="glyphicon glyphicon-thumbs-down"></span>
    <span class="action-desc" style="display:none;">{{ Lang::get('site.dislike') }}</span>
    <span class="badge {{ Str::slug($pleasant->getDislikeUrl()) }}-count">
      {{ $pleasant->dislikeCount() }}
    </span>
    @if($pleasant->ajax or true)
    <script type="text/javascript">
      jQuery(document).ready(function() {
        jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }} .spinner").hide();

        jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }}").on('click',function(e){
          e.preventDefault();
          jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }} .glyphicon").hide();
          jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }} .spinner").show();

          jQuery.ajax("{{ $pleasant->getDislikeUrl() }}", {
            method: 'POST',
            dataType: 'json',
            success: function(response) {
              jQuery('.{{ Str::slug($pleasant->getLikeUrl()) }}-count').html(response.likes);
              jQuery('.{{ Str::slug($pleasant->getDislikeUrl()) }}-count').html(response.dislikes);

            },
            complete: function() {
              jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }} .glyphicon").show();
              jQuery("#{{ Str::slug($pleasant->getDislikeUrl()) }} .spinner").hide();
            },
          });
        });
      });
    </script>
    @endif
  </button>
</div>
  <br /><br /><small class="disclaimer">{{Lang::get('site.disclaimer')}}</small>
</div>
</div>
